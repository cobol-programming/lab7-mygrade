       IDENTIFICATION DIVISION.
       PROGRAM-ID. MY-GRADE.
       AUTHOR. KANTIDA.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       
       FILE-CONTROL. 
           SELECT MYGRADE-FILE ASSIGN TO "mygrade.txt"
           ORGANIZATION IS LINE SEQUENTIAL.

           SELECT RESULT-FILE ASSIGN TO "avg.txt"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION. 
       FD MYGRADE-FILE.
       01 GRADE-DETAIL.
           88 END-OF-SCORE-FILE VALUE HIGH-VALUE .
           05 ID-COURSE PIC X(6).
           05 NAME-COURSE PIC X(50).
           05 CREDIT PIC 9 VALUE ZERO.
           05 GRADE PIC X(2).

       FD RESULT-FILE.
       01 RESULT-DETAIL.
           05 AVG-GRADE PIC 9V9(2) .
           05 AVG-GRADE-SCI PIC 9V9(2) .
           05 AVG-GRADE-CS PIC 9V9(2) .
       
       WORKING-STORAGE SECTION. 
       01 NUM-GRADE PIC 9V9(2) VALUE ZERO .
       01 AVG-ALL-GRADE.
           05 ALL-SCORE-GRADE PIC 9(3)V9(2) VALUE ZERO .
           05 ALL-CREDIT PIC 9(3)V9(2) VALUE ZERO .
           05 AVG-GRADES PIC 9V9(2) VALUE ZERO .

       01 AVG-SCI-GRADE.
           05 ALL-SCORE-SCI PIC 9(3)V9(2) VALUE ZERO .
           05 ALL-CREDIT-SCI PIC 9(3)V9(2) VALUE ZERO .
           05 AVG-GRADE-SCI PIC 9V9(2) VALUE ZERO .

       01 AVG-CS-GRADE.
           05 ALL-SCORE-CS PIC 9(3)V9(2) VALUE ZERO .
           05 ALL-CREDIT-CS PIC 9(3)V9(2) VALUE ZERO .
           05 AVG-GRADE-CS PIC 9V9(2) VALUE ZERO .

       01 TWO-DIGIT-ID-COURSE PIC XX.
       01 FIRSE-DIGIT-ID-COURSE PIC X.


       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT MYGRADE-FILE 
           OPEN OUTPUT RESULT-FILE
           
           PERFORM UNTIL END-OF-SCORE-FILE 
              READ MYGRADE-FILE AT END SET END-OF-SCORE-FILE TO TRUE  
              END-READ
              IF NOT END-OF-SCORE-FILE THEN
                 PERFORM 001-PROCESS THRU 001-EXIT
              END-IF 
           END-PERFORM 

      *    GRADING PROCESS
           DISPLAY "-----------------------------"
           COMPUTE AVG-GRADES = ALL-SCORE-GRADE  / ALL-CREDIT 
           DISPLAY "AVG-GRADE: " AVG-GRADES
           DISPLAY "-----------------------------"

           COMPUTE AVG-GRADE-SCI IN AVG-SCI-GRADE 
                                      = ALL-SCORE-SCI / ALL-CREDIT-SCI
           DISPLAY "AVG-SCI-GRADE: " AVG-GRADE-SCI IN AVG-SCI-GRADE 
           DISPLAY "-----------------------------"

           COMPUTE AVG-GRADE-CS  IN AVG-CS-GRADE
                                   = ALL-SCORE-CS / ALL-CREDIT-CS 
           DISPLAY "AVG-CS-GRADE: " AVG-GRADE-CS IN AVG-CS-GRADE
           DISPLAY "-----------------------------"

           MOVE AVG-GRADES IN AVG-ALL-GRADE 
                    TO AVG-GRADE IN RESULT-DETAIL 

           MOVE AVG-GRADE-SCI IN AVG-SCI-GRADE 
                    TO AVG-GRADE-SCI IN RESULT-DETAIL              

           MOVE AVG-GRADE-CS  IN AVG-CS-GRADE 
                    TO AVG-GRADE-CS  IN RESULT-DETAIL  

           WRITE RESULT-DETAIL 
           CLOSE MYGRADE-FILE 
           CLOSE RESULT-FILE 
           GOBACK 
           .

       001-PROCESS.
           EVALUATE TRUE 
              WHEN GRADE = "A" MOVE 4.00 TO NUM-GRADE
              WHEN GRADE = "B+" MOVE 3.50 TO NUM-GRADE
              WHEN GRADE = "B" MOVE 3.00 TO NUM-GRADE
              WHEN GRADE = "C+" MOVE 2.50 TO NUM-GRADE
              WHEN GRADE = "C" MOVE 2.00 TO NUM-GRADE
              WHEN GRADE = "D+" MOVE 1.50 TO NUM-GRADE
              WHEN GRADE = "D" MOVE 1.00 TO NUM-GRADE
           END-EVALUATE 
           
           COMPUTE ALL-SCORE-GRADE  = (CREDIT * NUM-GRADE) 
                                         + ALL-SCORE-GRADE   
           COMPUTE ALL-CREDIT = CREDIT + ALL-CREDIT 

           MOVE ID-COURSE IN GRADE-DETAIL TO FIRSE-DIGIT-ID-COURSE 
           MOVE ID-COURSE IN GRADE-DETAIL TO TWO-DIGIT-ID-COURSE 

           IF FIRSE-DIGIT-ID-COURSE  = "3" THEN
              COMPUTE ALL-SCORE-SCI  = (CREDIT * NUM-GRADE) 
                                         + ALL-SCORE-SCI   
              COMPUTE ALL-CREDIT-SCI  = CREDIT + ALL-CREDIT-SCI

              IF TWO-DIGIT-ID-COURSE = "31" THEN
                 COMPUTE ALL-SCORE-CS  = (CREDIT * NUM-GRADE) 
                                            + ALL-SCORE-CS  
                 COMPUTE ALL-CREDIT-CS  = CREDIT + ALL-CREDIT-CS  
              END-IF 
               
           END-IF 
           .

       001-EXIT.
           EXIT.

       
              